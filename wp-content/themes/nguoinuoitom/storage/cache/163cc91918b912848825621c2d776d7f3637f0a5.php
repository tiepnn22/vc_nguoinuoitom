<div class="content-page">

  <article <?php echo post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php echo e(get_the_title()); ?></h1>
      <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </header>
    <div class="entry-content">
      <?php echo e(the_content()); ?>

    </div>

    <?php if(get_field('note') != '' && get_field('note') != null): ?>
    <div class="entry-note">
      <?php echo get_field('note'); ?>

    </div>
    <?php endif; ?>

    <?php echo $__env->make('partials.social-bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <footer>
      <?php echo $__env->make('partials.entry-meta-tags', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

    </footer>

    <?php echo e(comments_template('/views/partials/comments.blade.php')); ?>


    <?php 
    $postCate = get_the_category(get_the_ID());
    if (!empty($postCate)) {
      $cates = collect($postCate)->map(function ($item) {
        return $item->term_id;
      })->implode(',');
    }
    $catePosts = get_posts([
      'posts_per_page' => -1,
      'post__not_in' => [get_the_ID()],
      'cat' => $cates,
    ]);
     ?>

    <?php if(count($catePosts) > 0): ?>
    <div class="related-posts">
      <div class="main-title">
        <a>
          <h2><?php echo e(_e('Tin cùng chuyên mục', 'vicoders')); ?></h2>
        </a>
      </div>
      <?php echo do_shortcode('[listing per_page="5" cat="' . $cates . '" excludes="' . get_the_ID() . '"]'); ?>

    </div>
    <?php endif; ?>

  </article>
</div>

<div class="sidebar-small">
  <?php 
    dynamic_sidebar('single-post-small');
   ?>
</div>

<div class="sidebar-large">
  <?php 
    dynamic_sidebar('single-post-large');
   ?>
</div>