<div class="archive-list content-page">
	<div class="box-content">
		<?php echo $__env->make('partials.content-archive', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
</div>
<div class="sidebar-small">
  <?php (dynamic_sidebar('sidebar-small-home')); ?>
</div>
<div class="sidebar-large">
  <?php (dynamic_sidebar('sidebar-large-home')); ?>
</div>