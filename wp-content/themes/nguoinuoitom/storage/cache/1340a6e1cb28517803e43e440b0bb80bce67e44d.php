<div class="content-page content-page-video">

  <article <?php (post_class()); ?>>

    <div class="entry-content">
      <iframe width="420" height="315"
        src="https://www.youtube.com/embed/<?php echo e(get_field('youtube')); ?>">
      </iframe>
    </div>

    <header>
      <h1 class="entry-title"><?php echo e(get_the_title()); ?></h1>
      <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </header>

    <div class="entry-content">
      <?php (the_content()); ?>
    </div>

    <?php echo $__env->make('partials/social-bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <footer>
      <?php echo wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

    </footer>

    <?php (comments_template('/views/partials/comments.blade.php')); ?>

  </article>
</div>

<div class="sidebar-video">
  <?php (dynamic_sidebar('single-video')); ?>
</div>