<div class="content-page">

  <article <?php (post_class()); ?>>
    <header>
      <h1 class="entry-title"><?php echo e(get_the_title()); ?></h1>
      <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </header>
    <div class="entry-content">
      <?php (the_content()); ?>
    </div>

    <div class="entry-note">
      <?php echo get_field('note'); ?>

    </div>

    <footer>
      <?php echo $__env->make('partials.entry-meta-tags', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

    </footer>

    <?php (comments_template('/views/partials/comments.blade.php')); ?>

    <div class="related-posts">
      <div class="main-title">
        <a>
          <h2><?php echo e(_e('Tin cùng chuyên mục', 'vicoders')); ?></h2>
        </a>
      </div>
      <?php echo do_shortcode('[listing per_page="5" excludes="' . get_the_ID() . '"]'); ?>

    </div>

  </article>
</div>

<div class="sidebar-small">
  <?php (dynamic_sidebar('single-post-small')); ?>
</div>

<div class="sidebar-large">
  <?php (dynamic_sidebar('single-post-large')); ?>
</div>