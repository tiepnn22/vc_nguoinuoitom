<?php $__env->startSection('content'); ?>
    <div class="category-title">
        <h2><?php echo e(single_tag_title()); ?></h2>
    </div>

    <?php if(!have_posts()): ?>
    <div class="alert alert-warning">
        <?php echo e(__('Không tìm thấy bài viết nào.', 'vicoders')); ?>

    </div>
    <?php echo get_search_form(false); ?>

    <?php endif; ?>

    <?php while(have_posts()): ?> <?php (the_post()); ?>
        <?php echo $__env->make('partials.content-tag', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endwhile; ?>

  <?php echo get_the_posts_navigation(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>