<?php $__env->startSection('content'); ?>
    
    <?php 
    $cate = get_queried_object();
     ?>

    <div class="category-title">
            <h2><?php echo e($cate->name); ?></h2>
    </div>

    <div class="content-page">

        <?php if(!have_posts()): ?>
            <div class="alert alert-warning">
                <?php echo e(__('Xin lỗi, không tìm thấy kết quả nào.', 'sage')); ?>

            </div>
            <?php echo get_search_form(false); ?>

        <?php else: ?>
            <div class="featured-posts-widget">
                <?php 
                    $shortcode = '[listing cat="' . $cate->term_id . '" layout="partials.sections.widget-featured"]';
                    echo do_shortcode($shortcode);
                 ?>
            </div>

            <div class="recent-posts-slide">
                <?php 
                    $shortcode = '[listing categories="tin-tuc-quan-trong" layout="partials.sections.widget-slide"]';
                    echo do_shortcode($shortcode);
                 ?>
            </div>

            
            
            <div class="archive-list">

                <div class="main-title">
                    <a>
                        <h2><?php echo e(_e('Tin mới nhất', 'vicoders')); ?></h2>
                    </a>
                </div>
                
                <div class="box-content">
                
                    <?php echo do_shortcode('[listing cat="' . $cate->term_id . '" paged="yes" per_page="9" layout="partials.sections.archive-list"]'); ?>

                
                </div>
            </div>
        <?php endif; ?>

    </div>

    <div class="sidebar-small">
      <?php (dynamic_sidebar('category-small')); ?>
    </div>

    <div class="sidebar-large">
      <?php (dynamic_sidebar('category-large')); ?>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>