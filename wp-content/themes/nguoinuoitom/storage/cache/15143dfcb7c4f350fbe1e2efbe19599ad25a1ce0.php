<article class="item">
    <figure>
        <a href="<?php echo e($url); ?>">
            <img src="<?php echo e(asset('images/3x2.png')); ?>" 
                alt="<?php echo e($title); ?>" 
                style="background-image: url(<?php echo e($thumbnail); ?>);" 
            />
        </a>
    </figure>
    <div class="info">
        <div class="title">
            <a href="<?php echo e($url); ?>">
                <h3>
                    <?php echo e($title); ?>

                </h3>
            </a>
        </div>
        <div class="entry-updated">
            <?php echo e($published_date); ?>

        </div>
    </div>
</article>