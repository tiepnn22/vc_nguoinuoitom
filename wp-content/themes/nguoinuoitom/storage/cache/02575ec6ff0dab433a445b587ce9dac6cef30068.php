<footer class="footer">
    <div class="container">
        <?php (dynamic_sidebar('sidebar-footer')); ?>

        <div id="back-to-top">
		    <a href="javascript:void(0)">
		    	<!-- <i class="fa fa-chevron-up" aria-hidden="true"></i> -->
		    </a>
		</div>
    </div>
</footer>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5987c41bdbb01a218b4db054/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
