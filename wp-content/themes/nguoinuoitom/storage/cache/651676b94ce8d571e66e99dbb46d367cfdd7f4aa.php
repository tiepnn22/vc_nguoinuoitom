<div class="widget-category <?php echo e($theme); ?>" style="background: <?php echo e($background); ?>; 
    <?php 
        if(!empty($background)) { 
            echo "padding-right: 5px; padding-bottom: 5px;"; 
        } 
    ?>
">

    <div class="title-widget" style="<?php if(!empty($background)) { echo "padding: 5px;"; } ?>" >

    <?php if(is_string($url)): ?>

        <a href="<?php echo e($url); ?>">
            <h2>
                <?php echo e($title); ?>

            </h2>
        </a>

    <?php else: ?>

        <a href="<?php echo e(get_site_url()); ?>/videos">
            <h2>
                <?php echo e($title); ?>

            </h2>
        </a>

    <?php endif; ?>
    </div>


    <div class="<?php echo e($theme); ?>-content <?php if(!empty($background)) { echo "background"; } ?>" >

        <?php 

        switch ($theme) {
            case 'list':
                $layout = 'partials.sections.widget-list';
                break;
            case 'box':
                $layout = 'partials.sections.widget-box';
                break;
            case 'video':
                $layout = 'partials.sections.widget-video';
                break;
            case 'video_box':
                $layout = 'partials.sections.widget-video-box';
                break;
        }

        $shortcode = '[listing post_type="' . $posttype . '" cat="' . $cate . '" per_page="' . $number_post . '" layout="' . $layout . '"]';

        echo do_shortcode($shortcode);

         ?>

    </div>
</div>