<header class="header">
<!--     <a class="brand" href="<?php echo e(home_url('/')); ?>">
        <?php echo e(get_bloginfo('name', 'display')); ?>

    </a> -->
    <div class="header-top">
    	<div class="container">
    		<div class="logo">
    			<a href="<?php echo get_option('home');?>">
	            	<?php
						$header_logo = get_theme_mod( 'header-logo' );
						if( !empty($header_logo) ) {
							echo '<img src="'.$header_logo.'">';
						}
					?>
				</a>
			</div>
			<div class="header-ads">
				<div class="header-ads-left">
					<?php
						$header_ads_left = get_theme_mod( 'header-ads-left' );
						if( !empty($header_ads_left) ) {
							echo '<img src="'.$header_ads_left.'">';
						}
					?>
				</div>
				<div class="header-ads-right">
					<?php
						$header_ads_right = get_theme_mod( 'header-ads-right' );
						if( !empty($header_ads_right) ) {
							echo '<img src="'.$header_ads_right.'">';
						}
					?>
				</div>
			</div>
    	</div>
    </div>
	<nav class="menu">
		<div class="container">
	    	<div class="main-menu">
		        <?php if(has_nav_menu('main-menu')): ?>
		        <?php echo wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'menu-primary']); ?>

		        <?php endif; ?>
	        </div>
	        <div class="mobile-menu"></div>
		</div>
	</nav>
	<div class="header-marquee">
		<div class="container">
			<div class="marquee-date">
				<?php echo customDate(); ?>
			</div>

			<div class="runtext-container">
				<div class="main-runtext">
					<marquee direction="" onmouseover="this.stop();" onmouseout="this.start();">
						<div class="holder">
							<?php dynamic_sidebar('sidebar-top-news'); ?>
						</div>
					</marquee>
				</div>
			</div>

			<div class="marquee-hotline">
				| Hotline:01234567890 | <a href="<?php echo e(get_page_link(69)); ?>"><?php echo e(_e('Bạn đọc gửi bài', 'vicoders')); ?></a>
			</div>
		</div>
	</div>
</header>


