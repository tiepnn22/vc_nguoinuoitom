<?php if($count == 0): ?>
	<div>
        <a href="<?php echo e($url); ?>">
            <img src="<?php echo e(asset('images/3x2.png')); ?>" 
                alt="<?php echo e($title); ?>" 
                style="background-image: url(<?php echo e(getYoutubeImage(get_field('youtube', get_the_ID()))); ?>);background-size: 100%;width: 100%;height: 170px;" 
            />
        </a>
        <h5>
			<a href="<?php echo e($url); ?>">
				<span><?php echo e($title); ?></span>
			</a>
        </h5>
        <hr style="margin: 5px 0;">
	</div>
<?php else: ?>
	<a href="<?php echo e($url); ?>">
		<i class="fa fa-film" aria-hidden="true"></i>
		<span><?php echo e($title); ?></span>
	</a>
<?php endif; ?>