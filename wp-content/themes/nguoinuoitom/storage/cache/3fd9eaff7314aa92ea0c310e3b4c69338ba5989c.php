<article class="item">


	<figure>
        <a href="<?php echo e($url); ?>">
            <img src="<?php echo asset('images/3x2.png'); ?>" alt="<?php echo e($title); ?>" style="background-image: url(<?php echo e(getPostImage(get_the_ID())); ?>);" />
        </a>
		<div class="featured-title">
            <a href="<?php echo e($url); ?>">
            	<h3><?php echo e($title); ?></h3>
            </a>
        </div>
    </figure>

	<div class="info">
        <div class="desc">
            <?php echo e(createExcerptFromContent(get_the_excerpt(),40)); ?>

        </div>
    </div>


</article>


