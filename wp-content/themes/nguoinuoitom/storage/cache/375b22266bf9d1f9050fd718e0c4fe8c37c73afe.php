<article class="item">
    <figure>
        <a href="<?php echo e(the_permalink()); ?>">
            <img src="<?php echo e(asset('images/3x2.png')); ?>" 
                alt="<?php echo e(the_title()); ?>" 
                style="background-image: url(<?php echo e(getYoutubeImage(get_field('youtube', get_the_ID()))); ?>);" 
            />
        </a>
    </figure>
    <div class="info">
        <div class="title">
            <a href="<?php echo e(the_permalink()); ?>">
                <h3>
                    <?php echo e(the_title()); ?>

                </h3>
            </a>
        </div>
        <div class="entry-updated">
            <?php echo e(get_the_date('Y/m/d')); ?>

        </div>
    </div>
</article>