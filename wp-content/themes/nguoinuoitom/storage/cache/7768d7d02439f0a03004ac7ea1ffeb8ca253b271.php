<!-- <div>
	<img src="<?php echo e(getPostImage(get_the_ID())); ?>" alt="" />
	<h3><a href="<?php echo e($url); ?>"><?php echo e($title); ?></a></h3>
</div> -->


<article class="item">


	<figure>
        <a href="<?php echo e($url); ?>">
            <img src="<?php echo asset('images/3x2.png'); ?>" alt="<?php echo e($title); ?>" style="background-image: url(<?php echo e(getPostImage(get_the_ID())); ?>);" />
        </a>
    </figure>

	<div class="info">
		<div class="slide-title">
            <a href="<?php echo e($url); ?>">
            	<h3><?php echo e($title); ?></h3>
            </a>
        </div>
    </div>


</article>


