<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	
	<div class="subcate-details">
		<div class="title-widget">
			<a href="<?php echo e(get_category_link($cate->term_id)); ?>">
				<h2><?php echo e($cate->name); ?></h2>
			</a>
		</div>
		<!-- <h3><a href="<?php echo e(get_category_link($cate->term_id)); ?>"><?php echo e($cate->name); ?></a></h3> -->

		<?php echo do_shortcode('[listing cat="' . $cate->term_id . '" per_page="4"]'); ?>


	</div>
	
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>