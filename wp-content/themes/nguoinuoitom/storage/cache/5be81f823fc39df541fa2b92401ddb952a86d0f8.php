<article class="item">
	<figure>
		<a href="<?php echo e($url); ?>">
			<span class="youtube-icon"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
			<img src="<?php echo e(asset('images/3x2.png')); ?>" 
                alt="<?php echo e(the_title()); ?>" 
                style="background-image: url(<?php echo e(getYoutubeImage(get_field('youtube', $id))); ?>);" 
            />
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="<?php echo e($url); ?>">
				<h3><?php echo e($title); ?></h3>
			</a>
		</div>
		<div class="date">
			<?php echo e($publish_date); ?>

		</div>
	</div>
</article>