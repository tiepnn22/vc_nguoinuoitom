<?php if($count == 0): ?>
	<div class="content-page content-page-video featured-video">
		<iframe width="420" height="315"
			src="https://www.youtube.com/embed/<?php echo e(get_field('youtube', $id)); ?>">
		</iframe>
		<h1 class="entry-title"><?php echo e($title); ?></h1>
		<div class="entry-updated"><?php echo e($publish_date); ?></div>
	</div>

	<div class="sidebar-video">
		<div class="box-content">

<?php else: ?>
	<!-- <div class="list-featured-videos"> -->
			<article class="item">
				<figure>
					<a href="<?php echo e($url); ?>">
						<span class="youtube-icon"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
						<img src="<?php echo e(asset('images/3x2.png')); ?>" 
			                alt="<?php echo e($title); ?>" 
			                style="background-image: url(<?php echo e(getYoutubeImage(get_field('youtube', $id))); ?>);" 
			            />
					</a>
				</figure>
				
				<div class="title">
					<a href="<?php echo e($url); ?>">
						<h3><?php echo e($title); ?></h3>
					</a>
				</div>

				<div class="entry-updated">
					<?php echo e($publish_date); ?>

				</div>
			</article>
	<!-- </div> -->
	<?php if($count == $total - 1): ?>
		</div>
	</div>
	<?php endif; ?>
<?php endif; ?>