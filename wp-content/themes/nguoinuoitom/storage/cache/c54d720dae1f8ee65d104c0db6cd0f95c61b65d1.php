<p><time class="entry-updated" datetime="<?php echo e(get_post_time('c', true)); ?>"><?php echo e(get_the_date()); ?></time></p>

<p class="entry-excerpt"><?php echo e(get_the_excerpt()); ?></p>
