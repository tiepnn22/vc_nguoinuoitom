<div class="sidebar-small">
	<?php dynamic_sidebar('sidebar-small-home'); ?>
</div>

<div class="content-page">
	<?php echo $__env->make('partials.content-page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="featured-posts-widget">
            <?php 
            $shortcode = '[listing layout="partials.sections.widget-featured"]';
            echo do_shortcode($shortcode);
             ?>
        </div>

        <div class="recent-posts-slide">
            <?php 
                $shortcode = '[listing categories="tin-tuc-quan-trong" layout="partials.sections.widget-slide"]';
                echo do_shortcode($shortcode);
             ?>
        </div>

	<?php 

	$homeFields = get_field('list_category');

	 ?>


	<?php $__currentLoopData = $homeFields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

	<?php 
	$catePosts = get_posts(['post_type' => 'post', 'posts_per_page' => -1, 'cat' => $cate['category']]);
	$countPosts = count($catePosts);
	 ?>

	<?php if($countPosts > 0): ?>
	<div class="cat-home">
		<div class="main-title">
			<a href="<?php echo e(get_term_link($cate['category'])); ?>">
				<h2><?php echo e(get_cat_name($cate['category'])); ?></h2>
			</a>

			<?php 
			$childCates = get_term_children($cate['category'], 'category');
			 ?>

			<?php if(count($childCates) > 0): ?>

				<ul>
					<?php $__currentLoopData = $childCates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li><a href="<?php echo e(get_term_link($child)); ?>"><?php echo e(get_cat_name($child)); ?></a></li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>

			<?php endif; ?>

		</div>

		<?php 
        $shortcode = "[listing per_page=8 cat={$cate['category']} layout='partials.category-home']";
		echo do_shortcode($shortcode);
		 ?>

		<div class="content-ads">
			<a target="_blank" href="<?php echo e($cate['ads_link']); ?>"><img src="<?php echo e($cate['ads_image']['url']); ?>"></a>
		</div>

	</div>
	<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div>

<div class="sidebar-large">
	<?php dynamic_sidebar('sidebar-large-home'); ?>
</div>


