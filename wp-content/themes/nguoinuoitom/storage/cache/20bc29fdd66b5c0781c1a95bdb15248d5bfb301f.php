
<?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php if($key < $number_comments): ?>

		<div class="comment-row">
			<span class="comment-author"><?php echo e($comment->comment_author); ?>: </span>
			<a href="<?php echo e(get_permalink($comment->comment_post_ID)); ?>">
				<span class="comment-content"><?php echo e($comment->comment_content); ?></span>
			</a>
		</div>
		
	<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
