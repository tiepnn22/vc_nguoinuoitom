<article class="item">

	<?php if($count == 0): ?>
		<figure>
            <a href="<?php echo e($url); ?>">
                <img src="<?php echo asset('images/3x2.png'); ?>" alt="<?php echo e($title); ?>" style="background-image: url(<?php echo e($thumbnail); ?>);" />
            </a>
	    </figure>

	<?php endif; ?>

    <div class="info">
        <div class="title">
            <a href="<?php echo e($url); ?>">
            	<h3><?php echo e($title); ?></h3>
            </a>
        </div>

        <?php if($count == 0): ?>
            <div class="desc">
                <?php echo e($excerpt); ?>

            </div>
		<?php endif; ?>
    </div>

</article>


