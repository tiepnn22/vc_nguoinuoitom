<?php if($count == 0): ?>
	<div>
      <iframe width="420" height="315"
        src="https://www.youtube.com/embed/<?php echo e(get_field('youtube', $id)); ?>">
      </iframe>
	</div>
<?php else: ?>
	<a href="<?php echo e($url); ?>">
		<i class="fa fa-film" aria-hidden="true"></i>
		<span><?php echo e($title); ?></span>
	</a>
<?php endif; ?>