<div class="page-tag">
	<article <?php (post_class()); ?>>
	  	<header>
	  		<!-- <a href="<?php echo e(get_permalink()); ?>"> -->
	    		<h1 class="entry-title"><?php echo e(get_the_title()); ?></h1>
	    	<!-- </a> -->
	    	<?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	  	</header>
	  	<div class="entry-summary">
	    	<?php (the_excerpt()); ?>
	  	</div>
	</article>
</div>
