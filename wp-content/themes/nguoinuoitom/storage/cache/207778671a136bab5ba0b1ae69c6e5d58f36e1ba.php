<div class="item">
	<figure>
		<a href="<?php echo e(get_permalink()); ?>" title="<?php echo e(get_the_title()); ?>">
			
						<img src="<?php echo e(asset('images/3x2.png')); ?>" 
			                alt="<?php echo e($title); ?>" 
			                style="background-image: url(<?php echo e(getPostImage(get_the_ID())); ?>);" 
			            />
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="<?php echo e(get_permalink()); ?>" title="<?php echo e(get_the_title()); ?>">
				<h3>
					<?php echo e(get_the_title()); ?>

				</h3>
			</a>
		</div>
		<div class="desc">
			<?php echo e(createExcerptFromContent(get_the_excerpt(),20)); ?>

		</div>
	</div>
</div>