<div class="content-page-cate-video">
	<?php echo $__env->make('partials.content-page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<?php echo do_shortcode('[listing post_type="video" layout="partials.sections.video-featured" per_page="4"]'); ?>


	<div class="content-page-cate-list">
		<?php echo do_shortcode('[listing post_type="video" layout="partials.sections.video-list"]'); ?>

	</div>
</div>
