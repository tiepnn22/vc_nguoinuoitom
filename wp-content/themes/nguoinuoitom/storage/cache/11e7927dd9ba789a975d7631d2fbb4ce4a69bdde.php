<article class="item">
    <figure>
        <a href="<?php echo e(the_permalink()); ?>">
            <img src="<?php echo e(asset('images/3x2.png')); ?>" 
                alt="<?php echo e(the_title()); ?>" 
                style="background-image: url(<?php echo e(getPostImage(get_the_ID(), 'news-sidebar')); ?>);" 
            />
        </a>
    </figure>
    <div class="info">
        <div class="title">
            <a href="<?php echo e(the_permalink()); ?>">
                <h3>
                    <?php
                        // the_title();
                    ?>
                    <?php echo e(the_title()); ?>

                </h3>
            </a>
        </div>
        <div class="date">
            <?php
                // echo get_the_date('Y-m-d');
            ?>
            <?php echo e(get_the_date('Y-m-d')); ?>

        </div>
    </div>
</article>
<!-- <?php echo e(getPostImage(get_the_ID(), 'news-sidebar')); ?> -->