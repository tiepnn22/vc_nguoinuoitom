<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('partials.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php if(!have_posts()): ?>
    <div class="alert alert-warning">
        <?php echo e(__('Xin lỗi, không tìm thấy kết quả nào.', 'vicoders')); ?>

    </div>
    <?php echo get_search_form(false); ?>

    <?php endif; ?>

    <?php while(have_posts()): ?> <?php (the_post()); ?>
        <?php echo $__env->make('partials.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endwhile; ?>

    <?php echo get_the_posts_navigation(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>