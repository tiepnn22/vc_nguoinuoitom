<article class="item">
	<figure>
		<a href="<?php echo e($url); ?>">
			<img src="<?php echo e($thumbnail); ?>" alt="">
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="<?php echo e($url); ?>">
				<h3><?php echo e($title); ?></h3>
			</a>
		</div>
		<div class="desc">
			<?php echo e(createExcerptFromContent(get_the_excerpt(), 22)); ?>

		</div>
	</div>
</article>