<span><?php echo e(_e('Từ khoá', 'vicoders')); ?></span>
<?php 
	$tags = wp_get_post_tags(get_the_ID());

	$collectTags = collect($tags)->map(function ($tag) {
		echo '<a class="entry-tag" href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>';
	});
	
 ?>