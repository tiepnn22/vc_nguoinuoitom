<?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<div class="comment-row">
	<span class="comment-author"><?php echo e($comment->comment_author); ?>: </span>
	<span class="comment-content"><?php echo e($comment->comment_content); ?></span>
</div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>