import meanmenu from 'jquery.meanmenu/jquery.meanmenu.js';
// using this import type for require third 3rd. For example:
// import Wow from 'wow.js';
 
// or you can specify the script file which you want to import
// import meanmenu from 'jquery.meanmenu/jquery.meanmenu.js';

export default {
  init() {
    // JavaScript to be fired on all pages
    $('.main-menu').meanmenu({
        meanScreenWidth: "1030",
        meanMenuContainer: ".mobile-menu",
    });

    //back to top
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 20) {
            $("#back-to-top").css("display", "block");
        } else {
            $("#back-to-top").css("display", "none");
        }
    });
    if($('#back-to-top').length){
        $("#back-to-top").on('click', function() {
           $('html, body').animate({
               scrollTop: $('html, body').offset().top
             }, 1000);
        });
    }

    //slist sub cat home page
    // $('.cat-home-sub').click(function(){
    //   $(this).find('ul').stop(true,false).slideToggle();
    // });



    //search mobile
    $('.popup-plush').click(function(){
      $(this).parent().find('.popup-content').stop(true,false).slideToggle();
    });

    //slide home
    $('.featured-posts-widget > div').slick({
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        pauseOnHover: false,
        dots: false,
        arrows: true,
        prevArrow: '<span class="a-left control-c prev slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>',
        nextArrow: '<span class="a-right control-c next slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>',
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: false
            }
        }]
    });
    if ($('.recent-posts-slide .item').length > 3) {
        $('.recent-posts-slide > div').slick({
            infinite: true,
            speed: 1000,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 3000,
            pauseOnHover: false,
            dots: false,
            arrows: true,
            prevArrow: '<span class="a-left control-c prev slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>',
            nextArrow: '<span class="a-right control-c next slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>',
            responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            }, {
                breakpoint: 767,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }
            }, {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }
            }]
        });
    }


  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

    var preCommentArea = $('.pre-textarea');
    preCommentArea.change(function(){
      var comment = $(this).val();
      $('#comment').val(comment);
    });

    var preCommentForm = $('#pre_comment_form');
    preCommentForm.submit(function(e){
      e.preventDefault();
    });

    //$('.pre-textarea').fancybox();

    var fakeCommentBtn = $('#fake_comment');
    var fakeMessage = $('#fake_message');
    fakeCommentBtn.click(function (e) {
        var comment = preCommentArea.val();
        if (comment == '') {
            fakeMessage.text('Bạn chưa nhập nội dung bình luận.');
        } else {
            var commentForm = $('#commentform');
            $.fancybox.open(
                commentForm,
                {
                    type: 'inline'
                }
            );
        }
    });

    $('.background').niceScroll();
  },
};
