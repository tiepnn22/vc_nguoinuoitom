@extends('layouts.app')

@section('content')
    
    @php
    $cate = get_queried_object();
    @endphp

    <div class="category-title">
            <h2>{{ $cate->name }}</h2>
    </div>

    <div class="content-page">

        @if (!have_posts())
            <div class="alert alert-warning">
                {{ __('Xin lỗi, không tìm thấy kết quả nào.', 'sage') }}
            </div>
            {!! get_search_form(false) !!}
        @else
            <div class="featured-posts-widget">
                @php
                    $shortcode = '[listing cat="' . $cate->term_id . '" layout="partials.sections.widget-featured"]';
                    echo do_shortcode($shortcode);
                @endphp
            </div>

            <div class="recent-posts-slide">
                @php
                    $shortcode = '[listing categories="tin-tuc-quan-trong" layout="partials.sections.widget-slide"]';
                    echo do_shortcode($shortcode);
                @endphp
            </div>

            
            
            <div class="archive-list">

                <div class="main-title">
                    <a>
                        <h2>{{ _e('Tin mới nhất', 'vicoders') }}</h2>
                    </a>
                </div>
                
                <div class="box-content">
                
                    {!! do_shortcode('[listing cat="' . $cate->term_id . '" paged="yes" per_page="9" layout="partials.sections.archive-list"]') !!}
                
                </div>
            </div>
        @endif

    </div>

    <div class="sidebar-small">
      @php(dynamic_sidebar('category-small'))
    </div>

    <div class="sidebar-large">
      @php(dynamic_sidebar('category-large'))
    </div>

@endsection
