@extends('layouts.app')

@section('content')
    @while(have_posts()) @php(the_post())
        <!-- @include('partials.page-header') -->
        @if (is_home() || is_front_page())
        	@include('partials.home')
        @else
			@include('partials.content-page')
        @endif
    @endwhile
@endsection
