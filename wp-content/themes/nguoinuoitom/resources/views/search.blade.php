@extends('layouts.app')

@section('content')
    <div class="category-title">
        <h2>{{ _e('Tìm kiếm cho từ khóa: [' . $_GET['s'] . ']') }}</h2>
    }
    }
    </div>

    <div class="archive-list content-page">
        <div class="box-content">
            @if (!have_posts())
            <div class="alert alert-warning">
                {{  __('Không tìm thấy kết quả nào.', 'vicoders') }}
            </div>
            {!! get_search_form(false) !!}
            @endif

            @while(have_posts()) @php(the_post())
                @include('partials.content-archive')
            @endwhile
        </div>
    </div>

    <div class="sidebar-small">
      @php(dynamic_sidebar('sidebar-small-home'))
    </div>
    <div class="sidebar-large">
      @php(dynamic_sidebar('sidebar-large-home'))
    </div>

  {!! get_the_posts_navigation() !!}
@endsection
