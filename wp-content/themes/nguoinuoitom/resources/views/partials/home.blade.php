<div class="sidebar-small">
	<?php dynamic_sidebar('sidebar-small-home'); ?>
</div>

<div class="content-page">
	@include('partials.content-page')

        <div class="featured-posts-widget">
            @php
            $shortcode = '[listing layout="partials.sections.widget-featured"]';
            echo do_shortcode($shortcode);
            @endphp
        </div>

        <div class="recent-posts-slide">
            @php
                $shortcode = '[listing categories="tin-tuc-quan-trong" layout="partials.sections.widget-slide"]';
                echo do_shortcode($shortcode);
            @endphp
        </div>

	@php

	$homeFields = get_field('list_category');

	@endphp


	@foreach ($homeFields as $cate)

	@php
	$catePosts = get_posts(['post_type' => 'post', 'posts_per_page' => -1, 'cat' => $cate['category']]);
	$countPosts = count($catePosts);
	@endphp

	@if ($countPosts > 0)
	<div class="cat-home">
		<div class="main-title">
			<a href="{{ get_term_link($cate['category']) }}">
				<h2>{{ get_cat_name($cate['category']) }}</h2>
			</a>

			@php
			$childCates = get_term_children($cate['category'], 'category');
			@endphp

			@if (count($childCates) > 0)

				<ul>
					@foreach ($childCates as $child)
					<li><a href="{{ get_term_link($child) }}">{{ get_cat_name($child) }}</a></li>
					@endforeach
				</ul>

			@endif

		</div>

		@php
        $shortcode = "[listing per_page=8 cat={$cate['category']} layout='partials.category-home']";
		echo do_shortcode($shortcode);
		@endphp

		<div class="content-ads">
			<a target="_blank" href="{{ $cate['ads_link'] }}"><img src="{{ $cate['ads_image']['url'] }}"></a>
		</div>

	</div>
	@endif
	@endforeach

</div>

<div class="sidebar-large">
	<?php dynamic_sidebar('sidebar-large-home'); ?>
</div>


