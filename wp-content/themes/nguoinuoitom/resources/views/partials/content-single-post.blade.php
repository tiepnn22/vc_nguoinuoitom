<div class="content-page">

  <article {!! post_class() !!}>
    <header>
      <h1 class="entry-title">{{ get_the_title() }}</h1>
      @include('partials/entry-meta')
    </header>
    <div class="entry-content">
      {{ the_content() }}
    </div>

    @if (get_field('note') != '' && get_field('note') != null)
    <div class="entry-note">
      {!! get_field('note') !!}
    </div>
    @endif

    @include('partials.social-bar')

    <footer>
      @include('partials.entry-meta-tags')
      {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
    </footer>

    {{ comments_template('/views/partials/comments.blade.php') }}

    @php
    $postCate = get_the_category(get_the_ID());
    if (!empty($postCate)) {
      $cates = collect($postCate)->map(function ($item) {
        return $item->term_id;
      })->implode(',');
    }
    $catePosts = get_posts([
      'posts_per_page' => -1,
      'post__not_in' => [get_the_ID()],
      'cat' => $cates,
    ]);
    @endphp

    @if (count($catePosts) > 0)
    <div class="related-posts">
      <div class="main-title">
        <a>
          <h2>{{ _e('Tin cùng chuyên mục', 'vicoders') }}</h2>
        </a>
      </div>
      {!! do_shortcode('[listing per_page="5" cat="' . $cates . '" excludes="' . get_the_ID() . '"]') !!}
    </div>
    @endif

  </article>
</div>

<div class="sidebar-small">
  @php
    dynamic_sidebar('single-post-small');
  @endphp
</div>

<div class="sidebar-large">
  @php
    dynamic_sidebar('single-post-large');
  @endphp
</div>