<div class="item">
	<figure>
		<a href="{{ get_permalink() }}" title="{{ get_the_title() }}">
			{{-- <img src="{{ getPostImage(get_the_ID()) }}" alt="{{ get_the_title() }}" /> --}}
						<img src="{{ asset('images/3x2.png') }}" 
			                alt="{{ $title }}" 
			                style="background-image: url({{ getPostImage(get_the_ID()) }});" 
			            />
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="{{ get_permalink() }}" title="{{ get_the_title() }}">
				<h3>
					{{ get_the_title() }}
				</h3>
			</a>
		</div>
		<div class="desc">
			{{ createExcerptFromContent(get_the_excerpt(),20) }}
		</div>
	</div>
</div>