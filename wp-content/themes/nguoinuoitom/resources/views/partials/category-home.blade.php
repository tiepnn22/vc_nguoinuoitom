<article class="item">

	@if ($count == 0)
		<figure>
            <a href="{{ $url }}">
                <img src="<?php echo asset('images/3x2.png'); ?>" alt="{{ $title }}" style="background-image: url({{ $thumbnail }});" />
            </a>
	    </figure>

	@endif

    <div class="info">
        <div class="title">
            <a href="{{ $url }}">
            	<h3>{{ $title }}</h3>
            </a>
        </div>

        @if ($count == 0)
            <div class="desc">
                {{ $excerpt }}
            </div>
		@endif
    </div>

</article>


