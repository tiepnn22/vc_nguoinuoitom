@if ($count == 0)
	<div class="content-page content-page-video featured-video">
		<iframe width="420" height="315"
			src="https://www.youtube.com/embed/{{ get_field('youtube', $id) }}">
		</iframe>
		<h1 class="entry-title">{{ $title }}</h1>
		<div class="entry-updated">{{ $publish_date }}</div>
	</div>

	<div class="sidebar-video">
		<div class="box-content">

@else
	<!-- <div class="list-featured-videos"> -->
			<article class="item">
				<figure>
					<a href="{{ $url }}">
						<span class="youtube-icon"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
						<img src="{{ asset('images/3x2.png') }}" 
			                alt="{{ $title }}" 
			                style="background-image: url({{ getYoutubeImage(get_field('youtube', $id)) }});" 
			            />
					</a>
				</figure>
				
				<div class="title">
					<a href="{{ $url }}">
						<h3>{{ $title }}</h3>
					</a>
				</div>

				<div class="entry-updated">
					{{ $publish_date }}
				</div>
			</article>
	<!-- </div> -->
	@if ($count == $total - 1)
		</div>
	</div>
	@endif
@endif