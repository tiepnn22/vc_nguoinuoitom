<!-- <div>
	<img src="{{ getPostImage(get_the_ID()) }}" alt="" />
	<h3><a href="{{ $url }}">{{ $title }}</a></h3>
</div> -->


<article class="item">


	<figure>
        <a href="{{ $url }}">
            <img src="<?php echo asset('images/3x2.png'); ?>" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID()) }});" />
        </a>
    </figure>

	<div class="info">
		<div class="slide-title">
            <a href="{{ $url }}">
            	<h3>
                    <!-- {{ $title }} -->
                    {{ createExcerptFromContent(get_the_title(),8) }}
                </h3>
            </a>
        </div>
    </div>


</article>


