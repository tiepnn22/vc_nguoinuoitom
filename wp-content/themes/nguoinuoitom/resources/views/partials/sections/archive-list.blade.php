<article class="item">
	<figure>
		<a href="{{ $url }}">
			<img src="{{ $thumbnail }}" alt="">
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="{{ $url }}">
				<h3>{{ $title }}</h3>
			</a>
		</div>
		<div class="desc">
			{{ createExcerptFromContent(get_the_excerpt(), 22) }}
		</div>
	</div>
</article>