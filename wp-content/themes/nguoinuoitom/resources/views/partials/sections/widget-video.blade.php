@if ($count == 0)
	<div>
        <a href="{{ $url }}">
            <img src="{{ asset('images/3x2.png') }}" 
                alt="{{ $title }}" 
                style="background-image: url({{ getYoutubeImage(get_field('youtube', get_the_ID())) }});background-size: 100%;width: 100%;height: 170px;" 
            />
        </a>
        <h5>
			<a href="{{ $url }}">
				<span>{{ $title }}</span>
			</a>
        </h5>
        <hr style="margin: 5px 0;">
	</div>
@else
	<a href="{{ $url }}">
		<i class="fa fa-film" aria-hidden="true"></i>
		<span>{{ $title }}</span>
	</a>
@endif