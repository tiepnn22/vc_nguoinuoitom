<article class="item">
	<figure>
		<a href="{{ $url }}">
			<span class="youtube-icon"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
			<img src="{{ asset('images/3x2.png') }}" 
                alt="{{ the_title() }}" 
                style="background-image: url({{ getYoutubeImage(get_field('youtube', $id)) }});" 
            />
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="{{ $url }}">
				<h3>{{ $title }}</h3>
			</a>
		</div>
		<div class="entry-updated">
			{{ $publish_date }}
		</div>
	</div>
</article>