
@foreach ($comments as $key => $comment)
	@if($key < $number_comments)

		<div class="comment-row">
			<span class="comment-author">{{ $comment->comment_author }}: </span>
			<a href="{{ get_permalink($comment->comment_post_ID) }}">
				<span class="comment-content">{{ $comment->comment_content }}</span>
			</a>
		</div>
		
	@endif
@endforeach
