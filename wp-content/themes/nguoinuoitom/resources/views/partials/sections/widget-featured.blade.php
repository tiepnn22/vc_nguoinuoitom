<article class="item">


	<figure>
        <a href="{{ $url }}">
            <img src="<?php echo asset('images/3x2.png'); ?>" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID()) }});" />
        </a>
		<div class="featured-title">
            <a href="{{ $url }}">
            	<h3>{{ $title }}</h3>
            </a>
        </div>
    </figure>

	<div class="info">
        <div class="desc">
            {{ createExcerptFromContent(get_the_excerpt(),40) }}
        </div>
    </div>


</article>


