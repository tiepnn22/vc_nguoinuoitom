<article class="item">
    <figure>
        <a href="{{ the_permalink() }}">
            <img src="{{ asset('images/3x2.png') }}" 
                alt="{{ the_title() }}" 
                style="background-image: url({{ getYoutubeImage(get_field('youtube', get_the_ID())) }});" 
            />
        </a>
    </figure>
    <div class="info">
        <div class="title">
            <a href="{{ the_permalink() }}">
                <h3>
                    {{ the_title() }}
                </h3>
            </a>
        </div>
        <div class="entry-updated">
            {{ get_the_date('Y/m/d') }}
        </div>
    </div>
</article>