@foreach ($categories as $cate)
	
	<div class="subcate-details">
		<div class="title-widget">
			<a href="{{ get_category_link($cate->term_id) }}">
				<h2>{{ $cate->name }}</h2>
			</a>
		</div>
		<!-- <h3><a href="{{ get_category_link($cate->term_id) }}">{{ $cate->name }}</a></h3> -->

		{!! do_shortcode('[listing cat="' . $cate->term_id . '" per_page="4"]') !!}

	</div>
	
@endforeach