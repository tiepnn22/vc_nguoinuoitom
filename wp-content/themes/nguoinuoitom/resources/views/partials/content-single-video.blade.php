<div class="content-page content-page-video">

  <article @php(post_class())>

    <div class="entry-content">
      <iframe width="420" height="315"
        src="https://www.youtube.com/embed/{{ get_field('youtube') }}">
      </iframe>
    </div>

    <header>
      <h1 class="entry-title">{{ get_the_title() }}</h1>
      @include('partials/entry-meta')
    </header>

    <div class="entry-content">
      @php(the_content())
    </div>

    @include('partials/social-bar')
    
    <footer>
      {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
    </footer>

    @php(comments_template('/views/partials/comments.blade.php'))

  </article>
</div>

<div class="sidebar-video">
  <div class="box-content">
    @php(dynamic_sidebar('single-video'))
  </div>
</div>