<div class="widget-category {{ $theme }}" style="background: {{ $background }}; 
    <?php 
        if(!empty($background)) { 
            echo "padding-right: 5px; padding-bottom: 5px;"; 
        } 
    ?>
">

    <div class="title-widget" style="<?php if(!empty($background)) { echo "padding: 5px;"; } ?>" >

    @if (is_string($url))

        <a href="{{ $url }}">
            <h2>
                {{ $title }}
            </h2>
        </a>

    @else

        <a href="{{ get_site_url() }}/videos">
            <h2>
                {{ $title }}
            </h2>
        </a>

    @endif
    </div>


    <div class="{{ $theme }}-content <?php if(!empty($background)) { echo "background"; } ?>" >

        @php

        switch ($theme) {
            case 'list':
                $layout = 'partials.sections.widget-list';
                break;
            case 'box':
                $layout = 'partials.sections.widget-box';
                break;
            case 'video':
                $layout = 'partials.sections.widget-video';
                break;
            case 'video_box':
                $layout = 'partials.sections.widget-video-box';
                break;
        }

        $shortcode = '[listing post_type="' . $posttype . '" cat="' . $cate . '" per_page="' . $number_post . '" layout="' . $layout . '"]';

        echo do_shortcode($shortcode);

        @endphp

    </div>
</div>