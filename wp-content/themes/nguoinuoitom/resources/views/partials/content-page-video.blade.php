<div class="content-page-cate-video">
	@include('partials.content-page')
	
	{!! do_shortcode('[listing post_type="video" layout="partials.sections.video-featured" per_page="4"]') !!}

	<div class="content-page-cate-list">
		{!! do_shortcode('[listing post_type="video" layout="partials.sections.video-list"]') !!}
	</div>
</div>
