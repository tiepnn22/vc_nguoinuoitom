<div class="social-bar" style="margin: 15px 0 0;">

    <div class="btgg" style="margin-right: 5px; display: inline-block; float: left;">
        <div class="g-plusone" data-count="true" data-size="medium" data-href='{{ the_permalink() }}'></div>
    </div>

    <div class="btfbl" style="display: inline-block; float: left;">
        <div class="fb-like" data-href="{{ the_permalink() }}" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
    </div>

    <div style="margin-left: 4px; display: inline-block; float: left;" class="zalo-share-button" data-href="{{ the_permalink() }}" data-oaid="776714595574539203" data-layout="1" data-color="blue" data-customize="false" ></div>

    <div class="print" style="margin-left: 4px; display: inline-block; float: left;">
        <button style="border: 1px solid #ccc; background: #ffffff; margin-top: -1px; padding: 0px 5px;" onclick="window.print()">
            <i class="fa fa-print" aria-hidden="true"></i> In
        </button>
    </div>

    <div style="clear: both;"></div>
</div>


<script type="text/javascript">
    window.___gcfg = { lang: 'vi' };
    (function () {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>