<?php
if (post_password_required()) {
  return;
}
?>

<section id="comments" class="comments">

  <?php if (have_comments()) : ?>
  
    <ol class="comment-list">
      <?php wp_list_comments(['style' => 'ol', 'short_ping' => true]); ?>
    </ol>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
      <nav>
        <ul class="pager">
          <?php if (get_previous_comments_link()) : ?>
            <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'vicoders')); ?></li>
          <?php endif; ?>
          <?php if (get_next_comments_link()) : ?>
            <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'vicoders')); ?></li>
          <?php endif; ?>
        </ul>
      </nav>
    <?php endif; ?>
  <?php endif; ?>

  <?php if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments')) : ?>
    <div class="alert alert-warning">
      <?php _e('Comments are closed.', 'vicoders'); ?>
    </div>
  <?php endif; ?>

  <div class="pre-comment-form">
    <form id="pre_comment_form">
      <textarea class="pre-textarea" placeholder="<?php _e('Ý kiến của bạn...', 'vicoders'); ?>"></textarea>
      <input id="fake_comment" type="submit" name="submit" value="<?php _e('Gửi ý kiến', 'vicoders'); ?>" />
      <span class="error" id="fake_message"></span>
    </form>
  </div>

  <?php

  $fields = [
    'email' => '<p class="comment-form-email"><span class="required">*</span><label for="email">' . __('Email (không hiển thị trên trang):', 'vicoders') . '</label><input id="email" name="email" type="text" /></p>',
    'author' => '<p class="comment-form-author"><span class="required">*</span><label for="author">' . __('Họ tên (hiển thị trên trang):', 'vicoders') . '</label><input id="author" name="author" type="text" /></p><p>Vui lòng nhập mã xác nhận vào ô bên dưới. Nếu bạn không đọc được, hãy Chọn mã xác nhận khác.</p>'
  ];

  $commentArgs = [
    'comment_notes_before' => '',
    'title_reply' => '',
    'comment_field' => '<textarea style="display: none;" id="comment" name="comment" placeholder="' . __('Ý kiến của bạn', 'vicoders') . '"></textarea>',
    'label_submit' => __('Gửi', 'vicoders'),
    'fields' => $fields
  ];

  comment_form($commentArgs);

  ?>

</section>
