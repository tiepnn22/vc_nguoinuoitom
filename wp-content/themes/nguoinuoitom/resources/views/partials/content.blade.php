<div class="page-tag">
	<article @php(post_class())>
	  	<header>
	  		<!-- <a href="{{ get_permalink() }}"> -->
	    		<h1 class="entry-title">{{ get_the_title() }}</h1>
	    	<!-- </a> -->
	    	@include('partials/entry-meta')
	  	</header>
	  	<div class="entry-summary">
	    	@php(the_excerpt())
	  	</div>
	</article>
</div>
