@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Xin lỗi, nhưng trang bạn đang xem không tồn tại.', 'vicoders') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  {!! get_the_posts_navigation() !!}
@endsection
