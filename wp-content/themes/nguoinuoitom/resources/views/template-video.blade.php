{{--
  Template Name: Video Template
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php(the_post())
        @include('partials.page-header')
        @include('partials.content-page-video')
    @endwhile
@endsection
