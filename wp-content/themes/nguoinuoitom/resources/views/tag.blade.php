@extends('layouts.app')

@section('content')


    @if (!have_posts())
        <div class="alert alert-warning">
            {{  __('Không tìm thấy bài viết nào.', 'vicoders') }}
        </div>
        {!! get_search_form(false) !!}
    @endif

    <div class="archive-list content-page">

        <div class="category-title">
            <h2>{{ single_tag_title() }}</h2>
        </div>

        <div class="box-content">
            

            @while(have_posts()) @php(the_post())
                @include('partials.content-archive')
            @endwhile

        </div>
    </div>

    <div class="sidebar-small">
      @php(dynamic_sidebar('sidebar-small-home'))
    </div>
    
    <div class="sidebar-large">
      @php(dynamic_sidebar('sidebar-large-home'))
    </div>

  {!! get_the_posts_navigation() !!}
@endsection
