<?php
//namespace App;

if (!function_exists('view')) {
    /**
     * [view description]
     * @param  [type]  $path [description]
     * @param  array   $data
     * @param  boolean $echo [description]
     * @return [type]        [description]
     */
    function view($path, $data = [], $echo = true)
    {
        if ($echo) {
            echo NF\View\Facades\View::render($path, $data);
        } else {
            return NF\View\Facades\View::render($path, $data);
        }
    }
}

if (!function_exists('asset')) {
    /**
     * [asset description]
     * @param [type] $assets [description]
     */
    function asset($assets)
    {
        return wp_slash(dirname(get_stylesheet_directory_uri()) . '/dist/' . $assets);
    }
}

if (!function_exists('title')) {
    /**
     * 
     * @return string
     */
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name . ' - ' . get_bloginfo('name');
        }

        if (is_404()) {
            return '404 page not found - ' . get_bloginfo('name');
        }

        return get_the_title() . ' - ' . get_bloginfo('name');
    }
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filterTemplates($templates)
{
    return collect($templates)
        ->map(function ($template) {
            return preg_replace('#\.(blade\.)?php$#', '', ltrim($template));
        })
        ->flatMap(function ($template) {
            $paths = apply_filters('templates/filter_templates/paths', ['views', 'resources/views']);
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                        "{$template}.blade.php",
                        "{$template}.php",
                    ];
                });
        })
        ->filter()
        ->unique()
        ->all();
}

if (!function_exists('createExcerptFromContent')) {
    /**
     * this function will create an excerpt from post content
     * 
     * @param  string $content
     * @param  int    $limit
     * @param  string $readmore
     * @since  1.0.0
     * @return string $excerpt
     */
    function createExcerptFromContent($content, $limit = 50, $readmore = '...')
    {
        if (!is_string($content)) {
            wp_die(__('createExcerptFromContent: first parameter must be a string.', ''));
        }

        if (!is_int($limit)) {
            wp_die(__('createExcerptFromContent: second parameter must be the number.', ''));
        }

        if ($limit <= 0) {
            wp_die(__('createExcerptFromContent: second parameter must greater than 0.'));
        }

        if ($content == '') {
            return '';
        }

        $words = explode(' ', $content);

        if (count($words) <= $limit) {
            $excerpt = $words;
        } else {
            $excerpt = array_chunk($words, $limit)[0];
        }

        return strip_tags(implode(' ', $excerpt)) . $readmore;
    }
}

if (!function_exists('getPostImage')) {
    /**
     * [getPostImage description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    function getPostImage($id, $imageSize = '')
    {
        //var_dump(asset('images/logo.png'));
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset('images/no-image.png') : $img[0];
    }
}

if (!function_exists('customDate')) {
    /**
     * 
     */
    function customDate($date = null)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $date = date('d/m/Y');

        $dayName = date('w');

        $out = '';
        switch ($dayName) {
            case 1:
                $out = 'Thứ Hai';
                break;
            case 2:
                $out = 'Thứ Ba';
                break;
            case 3:
                $out = 'Thứ Tư';
                break;
            case 4:
                $out = 'Thứ Năm';
                break;
            case 5:
                $out = 'Thứ Sáu';
                break;
            case 6:
                $out = 'Thứ Bảy';
                break;
            case 7:
                $out = 'Chủ Nhật';
                break;
            default:
                break;
        }

        return $datetime = $out . ',' . ' '. $date;
    }
}

if (!function_exists('getYoutubeImage')) {
    function getYoutubeImage($id)
    {
        $baseUrl = 'https://img.youtube.com/vi/';
        $imageUrl = $baseUrl . $id . '/0.jpg';
        return $imageUrl;
    }
}








