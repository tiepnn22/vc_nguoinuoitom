<?php

use MSC\Listing;
use MSC\ReCaptcha;
use MSC\PanelFactory;

/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style()
{
    wp_enqueue_style(
        'template-style',
        asset('styles/main.css'),
        false
    );
}

function theme_enqueue_scripts()
{
    wp_enqueue_script(
        'template-scripts',
        asset('scripts/main.js'),
        'jquery',
        '1.0',
        true
    );

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

    $params = array(
       'ajax_url' => admin_url('admin-ajax.php', $protocol)
    );

    wp_localize_script('template-scripts', 'ajax_obj', $params);
}

if (!function_exists('adminScript')) {

    function adminScript()
    {
        wp_enqueue_style('vicoders-admin-script', asset('styles/admin.css'));
    }

    add_action('admin_enqueue_scripts', 'adminScript');
}

if (!function_exists('themeSetup')) {
    /**
     * setup support for theme
     *
     * @return void
     */
    function themeSetup()
    {
        // Register menus
        register_nav_menus( array(
    		'main-menu' => __('Main Menu', 'vicoders')
    	) );

        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
        add_image_size('news', 188, 130, true);
        add_image_size('news-sidebar', 112, 80, true);
    }

    add_action('after_setup_theme', 'themeSetup');
}

if (!function_exists('themeSidebars')) {
    /**
     * register sidebar for theme
     *
     * @return void
     */
    function themeSidebars()
    {
        $sidebars = [
            [
                'name'          => __('Top News', 'vicoders'),
                'id'            => 'sidebar-top-news',
                'description'   => __('Top News', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Top ads', 'vicoders'),
                'id'            => 'sidebar-top-ads',
                'description'   => __('Top Ads', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Homepage: Small Sidebar', 'vicoders'),
                'id'            => 'sidebar-small-home',
                'description'   => __('Sidebar small home', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Homepage: Large Sidebar', 'vicoders'),
                'id'            => 'sidebar-large-home',
                'description'   => __('Sidebar Large Home', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Category: Small Sidebar', 'vicoders'),
                'id'            => 'category-small',
                'description'   => __('Category: Small Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Category: Large Sidebar', 'vicoders'),
                'id'            => 'category-large',
                'description'   => __('Category: Large Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Single Post: Small Sidebar', 'vicoders'),
                'id'            => 'single-post-small',
                'description'   => __('Single Post: Small Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Single Post: Large Sidebar', 'vicoders'),
                'id'            => 'single-post-large',
                'description'   => __('Single Post: Large Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Single Video', 'vicoders'),
                'id'            => 'single-video',
                'description'   => __('Single Video', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Sidebar Footer', 'vicoders'),
                'id'            => 'sidebar-footer',
                'description'   => __('Sidebar Footer', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Sidebar Mobile', 'vicoders'),
                'id'            => 'sidebar-mobile',
                'description'   => __('Sidebar Mobile', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Banner Home', 'vicoders'),
                'id'            => 'banner-home',
                'description'   => __('Banner Home', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }

    add_action('widgets_init', 'themeSidebars');
}


//shortcode có thể chạy được trong widget
add_filter('widget_text','do_shortcode');

//customizer
function customizer_a( $wp_customize ) {
    $wp_customize->add_section (
        'section_a',
        array(
            'title' => 'Khu vực customizer',
            'description' => 'Các tùy chọn cho Người nuôi tôm',
            'priority' => 1
        )
    );
/* text hotline header */
    $wp_customize->add_setting (
        'text_hotline_header',
        array(
            'default' => ' | Hotline:01234567890 | '
        )
    );
    $wp_customize->add_control (
        'text_hotline_header',
        array(
            'type' => 'text',
            'label' => 'Text hotline bạn đọc gửi bài',
            'section' => 'section_a',
            'settings' => 'text_hotline_header'
        )
    );
/* Image Upload */
    $wp_customize->add_setting( 'header-logo' );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'header-logo',
            array(
                'label' => 'Logo',
                'section' => 'section_a',
                'settings' => 'header-logo'
            )
        )
    );

}
add_action( 'customize_register', 'customizer_a' );

//Bộ lọc search
if( !is_admin() ) {
    function filter_search($query) {
        if ($query->is_search) {
            $query->set('post_type', array('post'));
        };
        return $query;
    };
    add_filter('pre_get_posts', 'filter_search');
}

// test for reCaptcha on comment form without images validation step
add_action('comment_form_after_fields', 'reCaptchaField');

function reCaptchaField($postId)
{
    if (is_user_logged_in()) return;

    echo '<div class="g-recaptcha" data-sitekey="6LcrIywUAAAAAFxple7tTAOWRugY2JrPUgY8oihX"></div>';
    echo '<script src="https://www.google.com/recaptcha/api.js?hl=vi"></script>';
}

add_filter('preprocess_comment', 'verifyCommentCaptcha');

function verifyCommentCaptcha($commentdata)
{
    if (is_user_logged_in()) return $commentdata;

    // your secret key
    $secret = "6LcrIywUAAAAAPEkhrSMWpzrwMv4jLlDzgp1l_vp";

    // empty response
    $response = null;

    // check secret key
    $reCaptcha = new ReCaptcha($secret);

    if (isset($_POST['g-recaptcha-response'])) {
        $response = $reCaptcha->verifyResponse(
            $_SERVER['REMOTE_ADDR'],
            $_POST['g-recaptcha-response']
        );

        if ($response != null && $response->success) {
            return $commentdata;
        } else {
            echo __('Vui lòng nhấp chọn lại để xác nhận bạn không phải robot.');
            return null;
        }

    } else {
        echo __('Vui lòng nhấp chọn lại để xác nhận bạn không phải robot.');
        return null;
    }
}

// update form validation message
add_filter('gform_validation_message', function ($message, $form) {
    return '';
}, 10, 2);

// update the individual field validation message
add_filter('gform_validation', 'customValidation');

function customValidation($validations)
{
    $form = $validations['form'];

    foreach ($form['fields'] as $key => &$field) {
        $field['validation_message'] = 'Bạn phải nhập trường này';
    }

    $validations['form'] = $form;

    return $validations;
}

// add counting post
add_action('wp_head', function ($postId) {
    if (!is_single()) return;
    if (empty($postId)) {
        global $post;
        $postId = $post->ID;
    }

    setCountingViewPost($postId);
});

if (!function_exists('setCountingViewPost')) {
    function setCountingViewPost($postId)
    {
        $countingName = 'msc_view_count';
        $countNumber = get_post_meta($postId, $countingName, true);

        if ($countNumber == '') {
            $countNumber = 0;
            delete_post_meta($postId, $countingName);
            add_post_meta($postId, $countingName, 0);
        } else {
            $countNumber++;
            update_post_meta($postId, $countingName, $countNumber);
        }
    }
}

if (!function_exists('getCountingViewPost')) {
    function getCountingViewPost($postId)
    {
        $countingName = 'msc_view_count';
        return get_post_meta($postId, $countingName, true);
    }
}

/**
 * display full buttons of TinyMCE Editor
 * 
 * @param  array $buttons
 * @return array $buttons
 */
function enableButtonsTinyMCE($buttons)
{
    $buttons[] = 'fontselect';
    $buttons[] = 'fontsizeselect';
    $buttons[] = 'styleselect';
    $buttons[] = 'backcolor';
    $buttons[] = 'newdocument';
    $buttons[] = 'cut';
    $buttons[] = 'copy';
    $buttons[] = 'charmap';
    $buttons[] = 'hr';
    $buttons[] = 'visualaid';
    $buttons[] = 'justify';
 
    return $buttons;
}
 
add_filter('mce_buttons_3', 'enableButtonsTinyMCE');

/**
 * keep kitchen always on
 * 
 * @param  array $open
 * @return array $open
 */
function keepTinyMceOn($open)
{
 
    $open['wordpress_adv_hidden'] = FALSE;
 
    return $open;
}
 
add_filter('tiny_mce_before_init', 'keepTinyMceOn');

/**
 * import script into after opening <body> tag
 * @todo this approarch need to be improve
 */
add_filter('body_class', 'fanpageScript', PHP_INT_MAX);

function fanpageScript($classes)
{
    $appId = '258036011273045';
    $lang = 'vi_VN';

    $script = <<<EOT
"><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/{$lang}/sdk.js#xfbml=1&version=v2.10&appId={$appId}";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<noscript></noscript novar="
EOT;
    
    $classes[] = $script;

  return $classes;
}

