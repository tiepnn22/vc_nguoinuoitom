<?php

namespace App\AdminPages;

class NewsAdminPage
{
	public function __construct()
	{
		add_action('admin_menu', [$this, 'registerNewsPage']);
		$this->save();
	}

	public function registerNewsPage()
	{
	    add_menu_page(
	    	'Quản lý tác vụ', 
	    	'Quản lý tác vụ', 
	    	'manage_options', 
	        'news_settings', 
	        [$this, 'createMainNewsPage']
		);
	         
	    add_submenu_page(
	    	'news_settings', 
	        'Tin tức', 
	        'Tin tức', 
	        'manage_options', 
	        'news_settings', 
	        [$this, 'createNewsPage']
		);

	    add_submenu_page(
	    	'news_settings', 
	        'Thẻ Mua', 
	        'Thẻ Mua', 
	        'manage_options', 
	        'buy_tags_settings', 
	        [$this, 'createBuyTagsPage']
		);

	    add_submenu_page(
	    	'news_settings', 
	        'Thẻ Bán', 
	        'Thẻ Bán', 
	        'manage_options', 
	        'sell_tags_settings', 
	        [$this, 'createSellTagsPage']
		);
	}

	public function createMainNewsPage()
	{
	}

	public function createNewsPage()
	{
		$news = get_option('news_items');
	    ?>
		<h1><?php _e('Quản lý tin tức', 'vicoders'); ?></h1>
		<p><?php _e('Đây là trang quản trị của phần tin tức chạy trên đầu website. Mỗi dòng là một tin tức mới. Bạn có thể thêm sửa xoá mỗi dòng và cập nhật vào cơ sở dữ liệu.', 'vicoders'); ?></p>
	    <div class="wrap">
	        <form method="POST" action="">
	            <h3><?php _e('Tin tức', 'vicoders'); ?></h3>
				<small><?php _e('Kéo thả để sắp xếp thứ tự hiển thị tin tức', 'vicoders'); ?></small>
	            <ul id="news_list">
				<?php
				if (!empty($news)) { 
					foreach ($news as $key => $item) {
						?>
				        <li class="news-item">
				        	<span class="order"><?php echo $key + 1 ?></span>
				        	<input type="text" class="regular-text" name="news-items[]" value="<?php echo esc_html($item); ?>">
				        	<a class="remove-news" href="#"><?php _e('Xoá Tin', 'vicoders'); ?></a>
				        </li>
						<?php
					}
				} else {
					?>
					<li id="no_news"><?php _e('Không có tin tức nào được thêm.', 'vicoders'); ?></li>
					<?php
				}
				?>
	            </ul>
	     
	            <a href="#" id="add_row"><?php _e('Thêm tin tức', 'vicoders'); ?></a>
	            <p>
	                <input type="submit" name="save_news" value="<?php _e('Lưu', 'vicoders'); ?>" class="button-primary"/>
	            </p>
	        </form>

			<span id="news_item_placeholder" style="display: none;">
		        <li>
		        	<span class="order">[...]</span>
		        	<input class="regular-text" type="text" name="news-items[]">
		        	<a class="remove-news" href="#"><?php _e('Xoá Tin', 'vicoders'); ?></a>
		        </li>
			</span>

	        <script type="text/javascript">
		        jQuery(document).ready(function () {
		        	var $ = jQuery;
		        	var newsList = $('#news_list');
		        	var noItems = $('#no_news');
		        	var addRowBtn = $('#add_row');
		        	addRowBtn.on('click', function () {
		        		noItems.remove();
		        		var newsItem = $('#news_item_placeholder').children('li').clone();
		        		newsList.append(newsItem);
		        		return false;
		        	});

		        	var removeRowBtn = $('.remove-news');
		        	$(document).on('click', '.remove-news', function () {
		        		$(this).parent().remove();
		        	});

					newsList.sortable({
						cursor: 'move'
					});
		        });
	        </script>
	    </div>
	    <?php
	}

	public function createBuyTagsPage()
	{
		$tags = get_tags();
		$dataTags = get_option('buy_tags_items');
	    ?>
		<h1><?php _e('Quản lý thẻ', 'vicoders'); ?></h1>
		<p><?php _e('Đây là trang quản lý thẻ.', 'vicoders'); ?></p>
	    <div class="wrap">
	        <form method="POST" action="">
	            <h3><?php _e('Thẻ mua', 'vicoders'); ?></h3>
				<small><?php _e('Kéo thả để sắp xếp thứ tự hiển thị các thẻ', 'vicoders'); ?></small>
	            <ul id="news_list">
				<?php
				if (!empty($dataTags)) { 
					foreach ($dataTags as $itemKey => $itemTag) {
						?>
				        <li class="news-item">
				        	<span class="order"><?php echo $itemKey + 1 ?></span>
				        	<select class="regular-text" name="buy-tag-items[]">
				        		<?php foreach ($tags as $key => $tag) : ?>
									<option value="<?php echo $tag->term_id; ?>" <?php selected($itemTag, $tag->term_id); ?>>
										<?php echo $tag->name; ?>
									</option>
				        		<?php endforeach; ?>
				        	</select>
				        	<a class="remove-news" href="#"><?php _e('Xoá Thẻ', 'vicoders'); ?></a>
				        </li>
						<?php
					}
				} else {
					?>
					<li id="no_news"><?php _e('Không có thẻ nào được thêm.', 'vicoders'); ?></li>
					<?php
				}
				?>
	            </ul>
	     
	            <a href="#" id="add_row"><?php _e('Thêm Thẻ', 'vicoders'); ?></a>
	            <p>
	                <input type="submit" name="save_buy_tags" value="<?php _e('Lưu', 'vicoders'); ?>" class="button-primary"/>
	            </p>
	        </form>

			<span id="news_item_placeholder" style="display: none;">
		        <li>
		        	<span class="order">[...]</span>
		        	<select class="regular-text" name="buy-tag-items[]">
		        		<?php foreach ($tags as $key => $tag) : ?>
							<option value="<?php echo $tag->term_id; ?>"><?php echo $tag->name; ?></option>
		        		<?php endforeach; ?>
		        	</select>
		        	<a class="remove-news" href="#"><?php _e('Xoá Tin', 'vicoders'); ?></a>
		        </li>
			</span>

	        <script type="text/javascript">
		        jQuery(document).ready(function () {
		        	var $ = jQuery;
		        	var newsList = $('#news_list');
		        	var noItems = $('#no_news');
		        	var addRowBtn = $('#add_row');
		        	addRowBtn.on('click', function () {
		        		noItems.remove();
		        		var newsItem = $('#news_item_placeholder').children('li').clone();
		        		newsList.append(newsItem);
		        		return false;
		        	});

		        	var removeRowBtn = $('.remove-news');
		        	$(document).on('click', '.remove-news', function () {
		        		$(this).parent().remove();
		        	});

					newsList.sortable({
						cursor: 'move'
					});
		        });
	        </script>
	    </div>
	    <?php
	}

	public function createSellTagsPage()
	{
		$tags = get_tags();
		$dataTags = get_option('sell_tags_items');
	    ?>
		<h1><?php _e('Quản lý thẻ', 'vicoders'); ?></h1>
		<p><?php _e('Đây là trang quản lý thẻ.', 'vicoders'); ?></p>
	    <div class="wrap">
	        <form method="POST" action="">
	            <h3><?php _e('Thẻ Bán', 'vicoders'); ?></h3>
				<small><?php _e('Kéo thả để sắp xếp thứ tự hiển thị các thẻ', 'vicoders'); ?></small>
	            <ul id="news_list">
				<?php
				if (!empty($dataTags)) { 
					foreach ($dataTags as $itemKey => $itemTag) {
						?>
				        <li class="news-item">
				        	<span class="order"><?php echo $itemKey + 1 ?></span>
				        	<select class="regular-text" name="sell-tag-items[]">
				        		<?php foreach ($tags as $key => $tag) : ?>
									<option value="<?php echo $tag->term_id; ?>" <?php selected($itemTag, $tag->term_id); ?>>
										<?php echo $tag->name; ?>
									</option>
				        		<?php endforeach; ?>
				        	</select>
				        	<a class="remove-news" href="#"><?php _e('Xoá Thẻ', 'vicoders'); ?></a>
				        </li>
						<?php
					}
				} else {
					?>
					<li id="no_news"><?php _e('Không có thẻ nào được thêm.', 'vicoders'); ?></li>
					<?php
				}
				?>
	            </ul>
	     
	            <a href="#" id="add_row"><?php _e('Thêm Thẻ', 'vicoders'); ?></a>
	            <p>
	                <input type="submit" name="save_sell_tags" value="<?php _e('Lưu', 'vicoders'); ?>" class="button-primary"/>
	            </p>
	        </form>

			<span id="news_item_placeholder" style="display: none;">
		        <li>
		        	<span class="order">[...]</span>
		        	<select class="regular-text" name="sell-tag-items[]">
		        		<?php foreach ($tags as $key => $tag) : ?>
							<option value="<?php echo $tag->term_id; ?>"><?php echo $tag->name; ?></option>
		        		<?php endforeach; ?>
		        	</select>
		        	<a class="remove-news" href="#"><?php _e('Xoá Thẻ', 'vicoders'); ?></a>
		        </li>
			</span>

	        <script type="text/javascript">
		        jQuery(document).ready(function () {
		        	var $ = jQuery;
		        	var newsList = $('#news_list');
		        	var noItems = $('#no_news');
		        	var addRowBtn = $('#add_row');
		        	addRowBtn.on('click', function () {
		        		noItems.remove();
		        		var newsItem = $('#news_item_placeholder').children('li').clone();
		        		newsList.append(newsItem);
		        		return false;
		        	});

		        	var removeRowBtn = $('.remove-news');
		        	$(document).on('click', '.remove-news', function () {
		        		$(this).parent().remove();
		        	});

					newsList.sortable({
						cursor: 'move'
					});
		        });
	        </script>
	    </div>
	    <?php
	}

	public function save()
	{
		$cleanInput = [];

		if (isset($_POST['save_news'])) {
			if (!empty($_POST['news-items'])) {
				foreach ($_POST['news-items'] as $row) {
					$cleanInput[] = $this->sanitize($row);
				}
			}

			update_option('news_items', $cleanInput);

			add_action('admin_notices', function () {
				?>
			    <div class="notice notice-success is-dismissible">
			        <p><?php _e('Lưu thành công', 'vicoders'); ?></p>
			    </div>
				<?php
			});
		}

		if (isset($_POST['save_buy_tags'])) {
			if (!empty($_POST['buy-tag-items'])) {
				foreach ($_POST['buy-tag-items'] as $row) {
					$cleanInput[] = $this->sanitize($row);
				}
			}

			update_option('buy_tags_items', $cleanInput);

			add_action('admin_notices', function () {
				?>
			    <div class="notice notice-success is-dismissible">
			        <p><?php _e('Lưu thành công', 'vicoders'); ?></p>
			    </div>
				<?php
			});
		}

		if (isset($_POST['save_sell_tags'])) {
			if (!empty($_POST['sell-tag-items'])) {
				foreach ($_POST['sell-tag-items'] as $row) {
					$cleanInput[] = $this->sanitize($row);
				}
			}

			update_option('sell_tags_items', $cleanInput);

			add_action('admin_notices', function () {
				?>
			    <div class="notice notice-success is-dismissible">
			        <p><?php _e('Lưu thành công', 'vicoders'); ?></p>
			    </div>
				<?php
			});
		}
	}

	public function sanitize($input)
	{
		return sanitize_text_field($input);
	}

}