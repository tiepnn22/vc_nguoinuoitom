<?php

namespace App\Widgets;

use MSC\Widget;

class RecentPostType extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('recent_post_type', 'textdomain'),
            'label'       => __('Recent Post Type', 'textdomain'),
            'description' => __('Lay ra cac bai viet moi nhat', 'textdomain'),
        ];

        $fields = [
            [
                'label' => __('Màu nền', 'textdomain'),
                'name'  => 'background',
                'type'  => 'text',
            ],
            [
                'label' => __('Số lượng bài viết hiển thị', 'textdomain'),
                'name'  => 'number_post',
                'type'  => 'number',
            ],
            [
                'label' => __('Chọn loại posttype', 'textdomain'),
                'name'  => 'posttype',
                'type'  => 'select',
                'options' => [
                    'post' => 'post',
                    'video' => 'video'
                ]
            ],
            [
                'label' => __('Chọn category', 'textdomain'),
                'name'  => 'id_category',
                'type'  => 'select',
                'options' => $this->getCategories()
            ],
            [
                'label' => __('Chọn taxonomy', 'textdomain'),
                'name'  => 'id_taxonomy',
                'type'  => 'number'
            ],
            [
                'label' => __('Chọn giao diện', 'textdomain'),
                'name'  => 'theme',
                'type'  => 'select',
                'options' => [
                	'list' => 'List',
                	'box' => 'Box',
                    'video' => 'Video',
                    'video_box' => 'Video Box'
                ]
            ],
            [
                'label' => __('Tiêu đề', 'textdomain'),
                'name'  => 'title',
                'type'  => 'text',
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function getCategories()
    {
		$categories = get_categories(array(
			'orderby' => 'name',
		));

		$cates[0] = __('Chọn Category', 'vicoders');

		foreach ($categories as $key => $value) {
			$cates[$value->term_id] = $value->name;
		}

		return $cates;
    }

    public function handle($instance)
    {
        // var_dump($instance);
        $data = [
            'background' => $instance['background'],
            'title' => $instance['title'],
            'posttype' => $instance['posttype'],
        	'theme' => $instance['theme'],
        	'url' => get_term_link(get_term($instance['id_category'])),
        	'cate' => $instance['id_category'],
            'taxo' => $instance['id_taxonomy'],
        	'number_post' => $instance['number_post']
        ];

        view('partials.widget-recent-post-type', $data);
	}
}