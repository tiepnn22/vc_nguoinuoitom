<?php

class search_sidebar extends WP_Widget {
    function __construct() {
        parent::__construct(
            'search_sidebar',
            'Search for sidebar',
            array( 'description'  =>  'Search trên sidebar' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Search',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';

    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $before_widget; ?>
            <form class="form-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <input type="text" placeholder="Tìm kiếm" name="s" value="<?php echo get_search_query(); ?>">
                <button type="submit" class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
        <?php echo $after_widget;
    }
}
function create_search_sidebar_widget() {
    register_widget('search_sidebar');
}
add_action( 'widgets_init', 'create_search_sidebar_widget' );
?>