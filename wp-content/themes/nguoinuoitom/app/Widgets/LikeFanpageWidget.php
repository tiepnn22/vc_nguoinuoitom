<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class LikeFanpageWidget extends Widget
{
    public function __construct()
    {
		$widget = [
			'id' => 'msc-like-fanpage',
			'label' => __('MSC Like Fanpage'),
			'description' => __('Like Fanpage Widget')
		];

		$fields = [
			[
		        'label' => 'Fanpage Url',
		        'name' => 'fanpage_url',
		        'type' => 'text',
			],
		];
		
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
		$shortcode = '[msc-like-fanpage fanpage_url="' . $instance['fanpage_url'] . '"]';
		echo do_shortcode($shortcode);
	}
}