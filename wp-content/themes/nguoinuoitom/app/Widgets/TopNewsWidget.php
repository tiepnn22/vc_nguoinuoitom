<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class TopNewsWidget extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('topnews', 'textdomain'),
            'label'       => __('Top News', 'textdomain'),
            'description' => __('Top News', 'textdomain'),
        ];

        $fields = [
            [
                'label' => 'Chọn bài viết',
                'name' => 'top_news',
                'type' => 'multiple_select',
                'options' => $this->getPosts()
            ]
        ];

        parent::__construct($widget, $fields);
    }

    public function getPosts()
    {
        $data = [];
        $posts = get_posts([
            'post_type' => 'post',
            'posts_per_page' => -1,
            'post_status' => 'publish'
        ]);

        if (!empty($posts)) {
            foreach ($posts as $key => $post) {
                $data[$post->ID] = $post->post_title;
            }
        }
    
        return $data;
    }

    public function handle($instance)
    {
        $news = get_option('news_items');
        if (!empty($news)) {
            foreach ($news as $item) {
                ?>
                 <div class="text-container">
                    <span><?php echo $item; ?></span>
                </div>
                <?php
            }
        }
	}
}
