<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class TagsWidget extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('tagswidget', 'textdomain'),
            'label'       => __('Thẻ', 'textdomain'),
            'description' => __('Thẻ', 'textdomain'),
        ];

        $fields = [
            [
                'label' => 'Loại thẻ',
                'name' => 'tag_type',
                'type' => 'select',
                'options' => [
                    'buy' => 'Mua',
                    'sell' => 'Bán'
                ]
            ]
        ];

        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        switch ($instance['tag_type']) {
            case 'buy':
                $tags = get_option('buy_tags_items');
                break;
            case 'sell':
                $tags = get_option('sell_tags_items');
                break;

            default:
                # code...
                break;
        }

        if (!empty($tags)) {
            echo '<div class="tagcloud">';
            foreach ($tags as $tagId) {
                $tag = get_term($tagId);
                echo '<a href="' . get_tag_link($tagId) . '">' . $tag->name . '</a>';
            }
            echo '</div>';
        }
	}
}
