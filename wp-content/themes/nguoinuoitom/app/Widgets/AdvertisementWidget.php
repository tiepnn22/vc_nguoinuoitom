<?php

namespace App\Widgets;

use MSC\Widget as Widget;

/**
 *
 */
class AdvertisementWidget extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('advertisement', 'vicoders'),
            'label'       => __('Quảng cáo', 'vicoders'),
            'description' => __('Quảng cáo', 'vicoders'),
        ];

        $fields = [
            [
                'label' => 'Chọn ảnh',
                'name' => 'ads_image',
                'type' => 'upload'
            ],
            [
                'label' => 'Link',
                'name' => 'ads_url',
                'type' => 'text'
            ],
        ];

        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>
        <a href="<?php echo $instance['ads_url']; ?>" target="_blank">
            <img src="<?php echo $instance['ads_image']; ?>" alt="">
        </a>
        <?php
	}
}
