<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class RecentCommentsWidget extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => 'recent_comments',
            'label'       => __('Recent Comments', 'vicoders'),
            'description' => __('Display recently comments on the post', 'vicoders'),
        ];

        $fields = [
            [
                'label' => __('Number comments', 'vicoders'),
                'name'  => 'number_comments',
                'type'  => 'number',
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        //var_dump($instance);

        $args = array('status'=>'approve');
        $comments = new \WP_Comment_Query($args);
        // echo '<pre>';
        // var_dump($comments->comments);
        // exit;

        $data = [
            'comments' => $comments->comments,
            'number_comments' => $instance['number_comments']
        ];

        view('partials.sections.widget-comments', $data);
	}
}
