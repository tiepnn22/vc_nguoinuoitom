<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class SubCategoryWidget extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('subcate-widget', 'textdomain'),
            'label'       => __('Sub Category', 'textdomain'),
            'description' => __('Sub Category', 'textdomain'),
        ];

        $fields = [
            ''
        ];

        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        $cate = get_queried_object();
        
        $subCates = get_categories(
            [
                'parent' => $cate->term_id,
            ]
        );

        view('partials.sections.widget-subcate', ['categories' => $subCates]);
	}
}
