<?php

namespace App\Taxonomies;

use MSC\Tax;

class VideoTaxonomy extends Tax
{
	public function __construct()
	{
		$config = [
			'slug' => 'video_cate',
			'single' => 'Video Category',
			'plural' => 'Video Categories'
		];

		$postType = 'video';

		$args = [

		];

		parent::__construct($config, $postType, $args);
	}
}