<?php

namespace App\Providers;

use App\AdminPages\NewsAdminPage;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    public $listen = [
        NewsAdminPage::class,
    ];

    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveAdminPage($class);
        }
    }

    /**
     * Resolve a widget instance from the class name.
     *
     * @param  string  $page
     * @return widget instance
     */
    public function resolveAdminPage($page)
    {
        return new $page();
    }
}
