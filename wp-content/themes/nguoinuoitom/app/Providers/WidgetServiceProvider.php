<?php

namespace App\Providers;

use App\Widgets\AdvertisementWidget;
use App\Widgets\RecentCommentsWidget;
use App\Widgets\RecentPostType;
use App\Widgets\SubCategoryWidget;
use App\Widgets\TagsWidget;
use App\Widgets\TopNewsWidget;
use App\Widgets\LikeFanpageWidget;
use App\Widgets\SampleWidget;

use Illuminate\Support\ServiceProvider;

class WidgetServiceProvider extends ServiceProvider
{
    public $listen = [
        RecentPostType::class,
        RecentCommentsWidget::class,
        SubCategoryWidget::class,
        TopNewsWidget::class,
        AdvertisementWidget::class,
        TagsWidget::class,
        LikeFanpageWidget::class,
        SampleWidget::class,
    ];

    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveWidget($class);
        }
    }

    /**
     * Resolve a widget instance from the class name.
     *
     * @param  string  $widget
     * @return widget instance
     */
    public function resolveWidget($widget)
    {
        return new $widget();
    }
}
