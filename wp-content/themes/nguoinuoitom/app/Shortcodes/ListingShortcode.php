<?php

namespace App\Shortcodes;

use MSC\Listing;
use MSC\View;

class ListingShortcode extends Listing
{
    public function __construct()
    {
        parent::__construct();
        $this->setCustomAttrs([
            'layout' => 'partials.sections.default-list'
        ]);
    }

	public function handle($query, $opts)
	{
        //var_dump($opts);
        //var_dump(count($query->found_posts));
        if ($query->have_posts()) {
        	$i = 0;
            while ($query->have_posts()) {
                $query->the_post();
                $view = new View;
                $data = [
                	'count'  => $i,
                    'id' => get_the_ID(),
                    'title' => get_the_title(),
                    'excerpt' => createExcerptFromContent(get_the_excerpt(), 22),
                    'url' => get_permalink(),
                    'thumbnail' => getPostImage(get_the_ID(), 'news'),
                    'content' => get_the_content(),
                    'publish_date' => get_the_date(),
                    'total' => count($query->posts),
                ];
                echo $view->render($opts['layout'], $data);
                $i++;
            }
        }
	}

}