<?php

namespace App\Shortcodes;

use NF\Abstracts\ShortCode;

class FanpageShortcode extends ShortCode
{
    public $name = 'msc-like-fanpage';

    public function render($attrs)
    {
		ob_start();

		$settings = shortcode_atts([
			'fanpage_url' => '',
			'fanpage_title' => '',
		], $attrs);

		?>
		<div class="fb-page" data-href="<?php echo $settings['fanpage_url']; ?>" data-tabs="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo $settings['fanpage_url']; ?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $settings['fanpage_url']; ?>"><?php echo $settings['fanpage_title']; ?></a></blockquote></div>

		<?php

		return ob_get_clean();
    }
}
