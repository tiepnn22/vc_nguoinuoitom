<?php

namespace App\Shortcodes;

use NF\Abstracts\ShortCode;

class PopularPostShortcode extends ShortCode
{
    public $name = 'popular_posts';

    public function render($attrs)
    {
    	ob_start();

        global $wpdb;

        $sql = "SELECT wp_statistics_pages.id, MAX(wp_statistics_pages.count) as view 
                FROM wp_statistics_pages 
                INNER JOIN wp_posts ON wp_statistics_pages.id=wp_posts.ID 
                WHERE wp_posts.post_type='post' 
                GROUP BY wp_statistics_pages.id
                ORDER BY view DESC
                LIMIT 0,5";

        $results = $wpdb->get_results($sql);

        // echo '<pre>';
        // var_dump($results);exit;

        $test = [];

        if (!empty($results)) {

            echo '<div class="box-content">';

            foreach ($results as $key => $result) {

                    $data = [
                        'view' => $result->view,
                        'count'  => $key,
                        'id' => $result->id,
                        'title' => get_the_title($result->id),
                        'excerpt' => createExcerptFromContent(get_the_excerpt($result->id), 10),
                        'url' => get_permalink($result->id),
                        'thumbnail' => getPostImage($result->id, 'news-sidebar'),
                        'content' => get_the_content($result->id),
                        'published_date' => get_the_date('d/m/Y', $result->id),
                        'total' => count($results),
                    ];

                    echo view('partials.sections.widget-box', $data);
                
            }
        }

        echo '</div>';

        return ob_get_clean();
    }
}
