<?php
namespace App\Shortcodes;

use NF\Abstracts\ShortCode;

class NodeSinglePostShortcode extends ShortCode
{
    public $name = 'ghichu';

    public function render($attr, $content)
    {
		// return '<span class="ghichu '.$attr['class'].' ">'.$content.'</span>';
		return '<span class="ghichu '.$attr['mau'].' '.$attr['class'].' ">'.$content.'</span>';
    }
}