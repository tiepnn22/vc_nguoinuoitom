<?php
/**
 * Video class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Video extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'video';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'Video';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'Videos';

    /**
     * $args optional
     * @var array
     */
    public $args = [
        'menu_icon' => 'dashicons-video-alt2',
        'supports' => [
            'title', 
            'editor',
            'author',
            'comments',
            'revisions'
        ]
    ];

}
