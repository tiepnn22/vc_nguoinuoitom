<?php
namespace NF\Interfaces;

interface TaxonomyInterface
{
    public function __construct();

    public function getArgs();

    public function createTaxonomy();

}
